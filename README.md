# Release Certification Reports
This repository is meant to contain the release certification issues created during the [release certification process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html).

Each report will live as an issue in the [issue tracker](https://gitlab.com/gitlab-org/jh-upstream-report/-/issues).
